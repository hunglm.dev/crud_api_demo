package com.hunglm.Entity;


import com.hunglm.Utils.StringUtils;

import javax.persistence.*;

/**
 * @author Minh Hung
 * the Entity Class
 * Mapped with the table Movie in the database
 * the ID is identity
 */
@Entity(name = "Movie")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String movieName;
    private long duration;
    @Column(length = 1000)
    private String description;
    private String status;
    private String thumbUrl;
    private String nameAlias;


    public Movie() {
    }

    public Movie(String movieName, long duration, String description) {
        this.movieName = movieName;
        this.duration = duration;
        this.description = description;
        this.status = "ACTIVE";
        this.thumbUrl = "https://drive.google.com/file/d/12J-OeqzBrsZIrP8Rtgu13VPvOJ2xMtmA/view?usp=sharing";
        this.nameAlias = StringUtils.convertUnicodeToEngString(movieName.toLowerCase());
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", movieName='" + movieName + '\'' +
                ", duration=" + duration +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", thumbUrl='" + thumbUrl + '\'' +
                ", nameAlias='" + nameAlias + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getNameAlias() {
        return nameAlias;
    }

    public void setNameAlias(String nameAlias) {
        this.nameAlias = nameAlias;
    }
}
