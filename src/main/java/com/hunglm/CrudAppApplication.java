package com.hunglm;

import com.hunglm.Utils.DBUtils;
import com.hunglm.services.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@SpringBootApplication
@EnableWebMvc
public class CrudAppApplication implements CommandLineRunner {
	@Autowired
	MovieService movieService;
	private static Logger logger = LoggerFactory.getLogger(CrudAppApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(CrudAppApplication.class, args);
	}

	@Override
	public void run(String... args) {
		logger.info("Try to init Sample data....");
		movieService.addAll(DBUtils.getDefaulList());
		logger.info("Init Sample Data done!!!");
	}
}
