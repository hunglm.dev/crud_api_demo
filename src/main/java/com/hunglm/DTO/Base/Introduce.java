package com.hunglm.DTO.Base;

/**
 * @author Minh Hung
 * the Class introduce
 * Don't care it muchs =))
 */
public class Introduce {

    private String method;
    private String contentType;
    private Object payload;
    private Class returnType;

    public Introduce() {
    }

    public Introduce(String method, String contentType, Object payload, Class returnType) {
        this.method = method;
        this.contentType = contentType;
        this.payload = payload;
        this.returnType = returnType;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    public Class getReturnType() {
        return returnType;
    }

    public void setReturnType(Class returnType) {
        this.returnType = returnType;
    }
}
