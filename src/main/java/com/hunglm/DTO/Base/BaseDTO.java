package com.hunglm.DTO.Base;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Minh Hung
 */
public class BaseDTO {
    @JsonProperty(value = "@Author")
    private final String author;
    @JsonProperty(value = "@Version")
    private final String version;

    public BaseDTO() {
        this.author = "hunglm";
        this.version = "0.0.1";
    }


    public String getAuthor() {
        return author;
    }

    public String getVersion() {
        return version;
    }
}
