package com.hunglm.DTO.Movie;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.hunglm.Entity.Movie;

/**
 * @author Minh Hung
 * the DTO Class
 * use for recieve or return the data of {@link Movie}
 */
public class MovieDTO {

    private Integer id;
    private String movieName;
    private Long duration;
    private String description;
    private String thumbUrl;
    private String status;

    @Override
    public String toString() {
        return "MovieDTO{" +
                "id=" + id +
                ", movieName='" + movieName + '\'' +
                ", duration=" + duration +
                ", description='" + description + '\'' +
                ", thumbUrl='" + thumbUrl + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    public boolean isNull(boolean includeId) {
        boolean res = isNull(movieName) || isNull(duration) || isNull(description) || isNull(thumbUrl);
        return includeId ? res || isNull(id) : res;
    }

    private boolean isNull(String input) {
        return input == null || input.trim().length() == 0;
    }

    private boolean isNull(Long input) {
        return input == null;
    }

    private boolean isNull(Integer input) {
        return input == null;
    }

    /**
     * convert DTO to Entity class
     * @return {@link Movie}
     */
    public Movie convertToMovie() {
        ObjectMapper mapper = new ObjectMapper();
        Movie movie = mapper.convertValue(this, Movie.class);
        if(isNull(status)){
            movie.setStatus("ACTIVE");
        }
        return movie;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
