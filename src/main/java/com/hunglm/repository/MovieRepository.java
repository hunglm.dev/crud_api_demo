package com.hunglm.repository;

import com.hunglm.Entity.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Minh Hung
 * the Repository Class
 * @see JpaRepository
 * the class Comunicate with the Database using {@link org.hibernate.Hibernate}
 */
@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {

    /**
     * Custom FindAll Method
     * @param pageable the Pagable
     * @return The Page of Movie
     * Using JPQL
     * <a href="https://docs.oracle.com/html/E13946_04/ejb3_langref.html>link</a>
     */
    @Query("FROM Movie m WHERE m.status NOT LIKE 'DELETED'")
    Page<Movie> findAll(Pageable pageable);

    @Query("FROM  Movie m WHERE m.id = :id AND m.status NOT LIKE 'DELETED'")
    Optional<Movie> findById(Integer id);

    @Query(value = "SELECT * FROM Movie m WHERE m.nameAlias ILIKE '%' || :name || '%' AND m.status NOT LIKE 'DELETED'"
            , nativeQuery = true)
    List<Movie> findByName(String name);
}
