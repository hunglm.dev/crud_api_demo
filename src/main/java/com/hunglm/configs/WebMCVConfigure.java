package com.hunglm.configs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author Minh Hung
 * Configuration Class
 * Extend {@link WebMvcConfigurerAdapter}
 * Add resource Handler to Enable Swagger Api
 */
@Configuration
@EnableWebMvc
public class WebMCVConfigure extends WebMvcConfigurerAdapter {

    private Logger logger = LoggerFactory.getLogger(WebMCVConfigure.class);
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        logger.info("#################Init Resource Handler");
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
