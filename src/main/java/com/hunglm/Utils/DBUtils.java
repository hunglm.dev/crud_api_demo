package com.hunglm.Utils;

import com.hunglm.DTO.Movie.MovieDTO;
import com.hunglm.Entity.Movie;

import java.util.ArrayList;
import java.util.List;

public class DBUtils {

    public static List<Movie> getDefaulList() {
        List<Movie> list = new ArrayList<>();
        String LOREM = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s," +
                " when an unknown printer took a galley of type and scrambled it to make a type specimen book." +
                " It has survived not only five centuries, but also the leap into electronic typesetting," +
                " remaining essentially unchanged.";
        list.add(new Movie("Na Tra: Ma Đồng Giáng Thế - Ne Zha", 65, LOREM));
        list.add(new Movie("Chuyến Tàu Sinh Tử - Train to Busan", 50, LOREM));
        list.add(new Movie("Yêu Miêu Truyện - Legend of the Demon Cat", 123, LOREM));
        list.add(new Movie("Hàng Xóm Tôi Là Totoro - My Neighbor Totoro", 155, LOREM));
        list.add(new Movie("Họa Bì - Painted Skin", 89, LOREM));
        list.add(new Movie("Vùng Đất Linh Hồn - Spirited Away", 92, LOREM));
        list.add(new Movie("Ngũ Hiệp Trừ Yêu - The Thousand Faces of Dunjia", 45, LOREM));
        list.add(new Movie("Thảm Họa Động Đất - The Quake", 200, LOREM));
        list.add(new Movie("5 Centimet Trên Giây - 5 Centimeters Per Second", 100, LOREM));
        list.add(new Movie("Mộ Đom Đóm - Grave Of The Fireflies", 148, LOREM));
        list.add(new Movie("Tân Vua Hài Kịch - The New King of Comedy", 63, LOREM));
        list.add(new Movie("Thiên Mệnh Anh Hùng - Blood Letter", 49, LOREM));
        list.add(new Movie("Truyền Thuyết Về Rồng - Tales from Earthsea", 123, LOREM));
        list.add(new Movie("5 Centimet Trên Giây - 5 Centimeters Per Second", 22, LOREM));
        return list;
    }

    public static MovieDTO getExampleValue(){
        MovieDTO movieDTO = new MovieDTO();
        movieDTO.setId(1);
        movieDTO.setDuration(120);
        movieDTO.setDescription("Description Lorem...");
        movieDTO.setMovieName("This is MOVIE NAME");
        movieDTO.setThumbUrl("http://yourThumbnailImageURL.yourDomain");
        return movieDTO;
    }

}
