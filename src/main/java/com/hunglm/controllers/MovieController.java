package com.hunglm.controllers;

import com.hunglm.DTO.Base.ResponseEntiy;
import com.hunglm.DTO.Movie.MovieDTO;
import com.hunglm.Entity.Movie;
import com.hunglm.Utils.Constant;
import com.hunglm.Utils.StringUtils;
import com.hunglm.services.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * @author Minh Hung
 * Rest API == Expose Api
 * {@link CrossOrigin} allow All origin
 */
@RestController
@CrossOrigin(origins = "*")
public class MovieController {

    @Autowired
    MovieService movieService;

    private static Logger logger = LoggerFactory.getLogger(BaseController.class);

    /**
     *
     * @param page the page Number.
     *             If dont pass it, The APi return All Movie
     * @return The list Movie
     */
    @GetMapping(value = "/getall")
    public ResponseEntiy<List<Movie>> index(Integer page) {
        logger.info("Call Get All, Page = {}", page);
        List<Movie> list;
        int pageSize = 5;
        if (page == null) {
            logger.info("Get UnPaged List!");
            list = movieService.getAll(Pageable.unpaged()).getContent();
        } else {
            list = movieService.getAll(PageRequest.of(page, pageSize)).getContent();
        }
        return ResponseEntiy.body(list);
    }

    /**
     *
     * @param id the Movie ID
     * @return the Movie if it exist
     * If the params id is not passed, Server return Bad Request
     */
    @GetMapping(value = "/findbyid")
    public ResponseEntiy<Object> findById(Integer id) {
        if (id == null) {
            return ResponseEntiy.body(Constant.BAD_REQUEST);
        }
        return ResponseEntiy.body(movieService.findById(id).orElse(null));
    }

    /**
     *
     * @param name the movie Name
     * @return the Movie
     * See docs of *findById*
     */
    @GetMapping(value = "/findbyname")
    public ResponseEntiy<Object> findByName(String name) {
        if (name == null) {
            return ResponseEntiy.body(Constant.BAD_REQUEST);
        }
        return ResponseEntiy.body(movieService.findByName(StringUtils.convertUnicodeToEngString(name)));
    }

    /**
     *
     * @param movieDTO the Data Transfer Object of the Movie
     * @return the added Result
     * @see ResponseEntiy
     */
    @PostMapping(value = "/add")
    public ResponseEntiy<Object> addMovie(MovieDTO movieDTO) {
        logger.info("AddMovie --- Payload= [{}]", movieDTO);
        logger.info("Cast to Movie: {}", movieDTO.convertToMovie());
        if (movieDTO.isNull(false)) {
            return ResponseEntiy.body(Constant.BAD_REQUEST);
        } else {
            //Make sure id is NULL to insert Entity
            movieDTO.setId(null);
            Movie movie = movieDTO.convertToMovie();

            return ResponseEntiy.body(movie);
        }
    }

    /**
     * the Delete Movie API
     * @param movieDTO the Movie DTO
     * @return the result
     * @see ResponseEntiy
     */
    @PostMapping(value = "/delete")
    public ResponseEntiy<Object> deleteMovie(MovieDTO movieDTO) {
        logger.info("Delete --- Payload= [{}]", movieDTO);
        if (movieDTO.getId() == null) {
            return ResponseEntiy.body(Constant.BAD_REQUEST);
        } else {
            Optional<Movie> movie = movieService.findById(movieDTO.getId());
            if (movie.isPresent()) {
                movie.get().setStatus("DELETED");
                return ResponseEntiy.body(movieService.save(movie.get()));
            } else {
                return ResponseEntiy.body(Constant.NOT_FOUND);
            }
        }
    }

    /**
     * the update Movie DTO
     * @param movieDTO the Movie DTO
     * @return the result
     * @see ResponseEntiy
     */
    @PostMapping(value = "update")
    public ResponseEntiy<Object> update(MovieDTO movieDTO) {
        if (movieDTO.isNull(true)) {
            return ResponseEntiy.body(Constant.BAD_REQUEST);
        } else {
            Optional<Movie> movie = movieService.findById(movieDTO.getId());
            if (movie.isPresent()) {
                Movie updateMovie = movieDTO.convertToMovie();
                updateMovie.setNameAlias(StringUtils.convertUnicodeToEngString(updateMovie.getMovieName()));
                return ResponseEntiy.body(movieService.save(updateMovie));
            } else {
                return ResponseEntiy.body(Constant.NOT_FOUND);
            }
        }
    }

}
