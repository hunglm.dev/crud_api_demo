package com.hunglm.controllers;

import com.hunglm.DTO.Base.ResponseEntiy;
import com.hunglm.Utils.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Minh Hung
 * Custom the response off Error
 * Replace the default WhiteLabel Page of Spring boot
 */
@RestController
public class MyErrController implements ErrorController {
    private static final Logger logger = LoggerFactory.getLogger(MyErrController.class);

    @RequestMapping("/error")
    public ResponseEntiy<Object> handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        Map<String, Object> map = new HashMap<>();
        logger.warn("ERROR: {}", status);
        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());
            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                map.put(Constant.ERR, Constant.NOT_FOUND);
            } else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                map.put(Constant.ERR, Constant.INTERAL_SERVER_ERROR);
            } else if (statusCode == HttpStatus.BAD_REQUEST.value()) {
                map.put(Constant.ERR, Constant.BAD_REQUEST);
            } else {
                map.put(Constant.ERR, Constant.UNEXPECTED_ERR);
            }
        }
        map.put(Constant.Exception.STATUS_CODE, request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE));
        map.put(Constant.Exception.MESSAGE, request.getAttribute(RequestDispatcher.ERROR_MESSAGE));
        map.put(Constant.Exception.URI, request.getAttribute(RequestDispatcher.ERROR_REQUEST_URI));
        return ResponseEntiy.body(map);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
