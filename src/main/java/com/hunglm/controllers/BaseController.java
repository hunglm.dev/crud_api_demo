package com.hunglm.controllers;

import com.hunglm.DTO.Base.Introduce;
import com.hunglm.DTO.Base.ResponseEntiy;
import com.hunglm.DTO.Movie.MovieDTO;
import com.hunglm.Entity.Movie;
import com.hunglm.Utils.Constant;
import com.hunglm.Utils.DBUtils;
import com.hunglm.services.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Minh Hung
 * Base Controller Class
 */
@RestController
@CrossOrigin(origins = "*")
public class BaseController {

    @Autowired
    MovieService movieService;

    private static Logger logger = LoggerFactory.getLogger(BaseController.class);

    //Introduce , Not care much =))

    /**
     * introduce API
     * Dont care it much
     * it can be replace by the swagger
     * @return Map<String, Object>
     */
    @GetMapping("/")
    public Map<String, Object> showAPI(){
        Map<String, Object> map = new HashMap<>();
        map.put("/resetDB", "Reset Sample data");
        map.put("/getall", new Introduce(Constant.Method.GET,
                "Not required",
                "[OPTIONAL] page(Integer) = pageNumber" , List.class));
        map.put("/findbyid", new Introduce(Constant.Method.GET, "Not required",
                Integer.class.toString() + " (id) = movieId", Movie.class));
        map.put("/findbyname", new Introduce(Constant.Method.GET, "Not required",
                String.class.toString() + " (name) = movieName", List.class));
        Map<String, Object> add = new HashMap<>();
        add.put("Class", MovieDTO.class);
        add.put("Example",DBUtils.getExampleValue());
        map.put("/add", new Introduce(Constant.Method.POST, MediaType.APPLICATION_FORM_URLENCODED.toString(),
                add , Movie.class));

        Map<String, Object> del = new HashMap<>();
        del.put("Class", MovieDTO.class);
        del.put("Example",DBUtils.getExampleValue());
        del.put("Note", "Require Only Property[id]");
        map.put("/delete", new Introduce(Constant.Method.POST, MediaType.APPLICATION_FORM_URLENCODED.toString(),
                del, Movie.class));

        Map<String, Object> update = new HashMap<>();
        update.put("Class", MovieDTO.class);
        update.put("Example",DBUtils.getExampleValue());
        update.put("Note", "Required ALL Properties");
        map.put("/update", new Introduce(Constant.Method.POST, MediaType.APPLICATION_FORM_URLENCODED.toString(),
                update, Movie.class));
        return map;
    }


    @GetMapping(value = "/resetDB")
    public ResponseEntiy<List<Movie>> test() {
        movieService.deleteAll();
        List<Movie> list = DBUtils.getDefaulList();
        return ResponseEntiy.body(movieService.addAll(list));
    }


}
